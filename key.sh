

dir=ssl
if [[ ! -d $dir ]];then
    mkdir -p ssl
fi
cd $dir
for pro in `cat pro.txt`;
	do
	  domain=$pro.dev.huwo.io
	  openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout $domain.key -out $domain.crt -subj "/CN=$domain/O=$domain"
	done
